<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Menu\CreateFormRequest;
use App\Http\Services\Menu\MenuService;
use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use function Ramsey\Uuid\v1;

class MenuController extends Controller
{
    protected $menuService;
    
    public function __construct(MenuService $menuService)
    {
        $this->menuService = $menuService;
    }

    public function create(){
        return view('admin.menu.add',[
            'title' => 'Thêm danh mục mới',
            'menus'=>$this->menuService->getParent()
        ]);
    }
    public function store(CreateFormRequest $request){
      $result=$this->menuService->create($request);
      return redirect()->back();
    }
    public function index(){
        return view('admin.menu.list',[
            'title' => 'Danh sách danh mục mới nhất',
            'menus'=>$this->menuService->getAll()
        ]);
    }

    public function destroy(Request $request){
        $result =$this->menuService->destroy($request);
        if($result){
            return response()->json([
                'status'=>'success',
                'message'=>'Xoá thành công',
                'error'=>false,

            ]);
        }
           return response()->json([
                'status'=>'fail',
                'message'=>'Xoá thất bại vui lòng kiểm tra lại',
                'error'=>true,

            ]);

            }
        public function show(Menu $menu){
            return view('admin.menu.edit',[
                'title' => 'Chỉnh sửa danh mục'.$menu->name,
                'menu'=>$menu,
               'menus'=>$this->menuService->getParent()
            ]);
        }
          public function update(Menu $menu,CreateFormRequest $request ){
            $this->menuService->update($request,$menu);
            return redirect()->route('list_danhmuc');
        }
}