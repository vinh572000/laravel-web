<?php

namespace App\Http\Services\Menu;

use App\Models\Menu;
use Illuminate\Contracts\Session\Session;
use PhpParser\Node\Stmt\TryCatch;
use Illuminate\Support\Str;

class MenuService
{
     public function getAll(){
        //  $ok=Menu::orderbyDesc('id')->paginate(20);
        //  dd($ok);   
        return Menu::orderby('id','asc')->paginate(20);
    }
    public function getParent(){
        return Menu::where('parent_id',0)->get();
    }
    public function create($request){
        try {
            Menu::create([
                'name'=>(string) $request->input('name'),
                'parent_id'=>(string) $request->input('parent_id'),
                'description'=>(string) $request->input('description'),
                'content'=>(string) $request->input('content'),
                'active'=>(string) $request->input('active'),
                'title'=>(string) $request->input('name'),
            ]);
             session()->flash('success', 'Tạo danh mục thành công');
             } catch (\Exception $err) {
                 session()->flash('error', $err->getMessage());
                 return false;
             }
                return true;
       
    }
    public function destroy($request){
        $id=$request->input('id');
        $menu=Menu::where('id',$id)->first();
        if($menu){
            return Menu::where('id',$id)->orWhere('parent_id',$id)->delete();
        }
        return false;
    }

    public function update($request,$menu):bool{
        if(!$request->input('parent_id')==$menu->id){ 
            $menu->fill($request->input());
            $menu->save();
        }
        session()->flash('success', 'Cập nhật danh mục thành công');
        return true;
    }
}