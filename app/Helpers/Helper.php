<?php



namespace App\Helpers;

class Helper{
    public static function menu($menus,$parent_id=0,$char=''){
        $html='';
        foreach($menus as $key=>$menu){
            if($menu['parent_id']==$parent_id){
                $html.='<tr>
                <td>'.$menu->id.'</td>
                <td>'.$char.$menu->name.'</td>
                <td>'.self::active($menu->active).'</td>
                <td>'.$menu->updated_at.'</td>
                <td>
                <a class="btn btn-primary btn-sm" href="/admin/menus/edit/'.$menu->id.'">
                <i class="fa fa-edit" aria-hidden="true"></i>
                </a>
                  <a class="btn btn-danger btn-sm" href="#"
                  onclick=removeRow('.$menu->id.',\'/admin/menus/destroy\')
                  >
                <i class="fa fa-trash" aria-hidden="true"></i>
                </a>
                </td>
                </tr>';
            }else{
                unset($menu[$key]);
              $html.='<tr>
                <td>'.$menu->id.'</td>
                <td>'.'--'.$menu->name.'</td>
                <td>'.self::active($menu->active).'</td>
                <td>'.$menu->updated_at.'</td>
                 <td>
                <a class="btn btn-primary btn-sm" href="/admin/menus/edit/'.$menu->id.'">
                <i class="fa fa-edit" aria-hidden="true"></i>
                </a>
                  <a class="btn btn-danger btn-sm" href="#"
                  onclick=removeRow('.$menu->id.',\'/admin/menus/destroy\')
                  >
                <i class="fa fa-trash" aria-hidden="true"></i>
                </a>
                </td>
                </tr>';
            }

             //$html .= self::menu($menus, $menu['id'], $char.'--');
             //$html.=self::menu($menus, $menu->id,$char.$menu->name.' - ');
        }
        return $html;
    }
    public static function active($active=0){
        return $active==0? '<span class="btn btn-danger btn-xs">NO</span>':
        '<span class="btn btn-success btn-xs">Yes</span>';
    }
}