@extends('admin.main')
@section('content')
    <form method="post">
        <div class="card-body">
            <div class="form-group">
                <label for="menu">Tên Danh Mục</label>
                <input type="text" name="menu" class="form-control" id="menu" placeholder="Enter name">
            </div>

            <div class="form-group">
                <label for="menu"> Danh Mục</label>
                <select class="form-control" name="parent_id">
                    <option value="0">Danh Mục Cha</option>
                </select>
            </div>
            <div class="form-group">
                <label for="menu">Mô tả</label>
                <textarea class="form-control" name="description" />
            </div>
            <div class="form-group">
                <label for="menu">Mô tả chi tiết</label>
                <textarea class="form-control" name="content" />
            </div>

        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
@endsection
